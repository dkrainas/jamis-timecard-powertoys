(function($){
	// Storage adapter - change to chrome.storage.local if no syncing desired
	var storage = chrome.storage.sync;
	var storageKey = "power_wons";
	var wons = [];
	
	var wonListHtml = '<div id="wonList"><div class="won-list-header"><a href="#" class="won-add" title="add a new won"><i class="icon-plus"></i></a><h3>Saved Work Orders</h3></div><div id="wons" class="won-list-body"></div></div>';
	
	var fontBootstrapper = "@font-face{font-family:'FontAwesome';src:url('"+chrome.extension.getURL("font/fontawesome-webfont.eot")+"');src:url('"+chrome.extension.getURL("font/fontawesome-webfont.eot")+"') format('embedded-opentype'),url('"+chrome.extension.getURL("font/fontawesome-webfont.woff")+"') format('woff'),url('"+chrome.extension.getURL("font/fontawesome-webfont.ttf")+"') format('truetype');font-weight:normal;font-style:normal}";
	
	function WonEntry(name, won){
		this.name = name;
		this.won = won;
	}
	
	function saveWons() {
		var wonData = {};
		wonData[storageKey] = wons;
	
		storage.set(wonData);
	}
		
	function getWonEntry(won) {
		for(var i = 0; i < wons.length; i++) {
			if(wons[i].won == won) 
				return wons[i];
		}
		
		return null;
	}
	
	function renderWonEntry(entry) {		
		return "<div class='won-entry' data-won='"+entry.won+"'><a href='#' title='insert WON "+entry.won+" into your time card' class='insert-won' data-won='"+entry.won+"'><i class='icon-double-angle-left'></i></a> "+entry.name+"<a href='#' title='options and actions' class='configure-won'><i class='icon-cog'></i></a><ul class='won-options'><li><a class='remove-won' title='remove entry' href='#'><i class='icon-trash'></i></a></li><li><a href='#' class='rename-won' title='rename entry'><i class='icon-edit'></i></a></li></ul></div>";
	}

	function isValid(entry) {
		return entry && entry.name && entry.won;
	}
	
	function renderAllWons() {
		$("#wons").empty();
		var html = "";
		for(var i = 0; i < wons.length; i++) {
			var w = wons[i];
			if(isValid(w)) {
				html += renderWonEntry(w);
			}
		}
		
		$("#wons").empty().append(html);
	}
	
	function insertWon(won) {
		var found = false;
		$("table table input[id$=txtWorkOrder1]").each(function() {
			if(!found) {
				var value = $(this).val();
				if(value == won) {
					found = true;
				}
				
				if(value == '') {
					$(this).val(won);
					$(this).focus().blur();
					found = true;
				}
				
				found && $(this).closest("tr").find("input[id$=txtDay3]").focus();
			}
		});
	};
	
	function addWon() {
		var won = prompt("Enter the WORK ORDER NUMBER for this entry");
		if(!won || won && won.length <= 0) {
			return;
		}
		
		var name = prompt("Enter a NAME for this work order number");
		if(name) {
			wons.push(new WonEntry(name, won));
			saveWons();
			renderAllWons();
		}
	}
	
	function removeWon(won) {
		if(confirm("Remove WON '"+won+"'?")) {
			var newWons = [];
			for(var i = 0; i < wons.length; i++) {
				var entry = wons[i];
				if(entry && entry.won != won) {
					newWons.push(entry);
				}
			}
			
			wons = newWons;
			saveWons();
			renderAllWons();
		}
	}
	
	function renameWon(won) {
		var entry = getWonEntry(won);
		if(entry && won) {
			var newName = prompt("Rename '"+entry.name+"' to what?");
			if(newName) {
				entry.name = newName;
				saveWons();
				renderAllWons();
			}
		}
	}
	
	// ui handlers
	function insertWonEvent(e) {
		var won = $(this).data("won");
		won && insertWon(won);
		e.preventDefault();
	};
	
	function addWonEvent(e) {
		addWon();
		e.preventDefault();
	};
	
	function removeWonEvent() {
		var won = $(this).parents(".won-entry").data("won");
		won && removeWon(won);
		e.preventDefault();
	}
	
	function renameWonEvent() {
		var won = $(this).parents(".won-entry").data("won");
		won && renameWon(won);
	}
	
	// function not in use
	function closeAllConfigMenusIfApplicable() {
		if($(this).parents("#wonList").size() <= 0) {
			resetAllConfigureMenus();
		}
	}
	
	function resetAllConfigureMenus() {
		$(".won-entry.configure").removeClass(".configure");
	}

	function openConfigureMenu() {
		resetAllConfigureMenus();
		$(this).parents(".won-entry").toggleClass("configure");
	}
	
	function refreshWonList(callback){
		storage.get(storageKey, function(items){
			if(typeof(items[storageKey]) !== 'undefined'){
			  wons = items[storageKey];
			}
            else
            {
                // If no sync data found, it's possible the user has an older version that used localStorage
                // If they did, get the stored wons the old way and put them in the sync storage
                var oldWons = localStorage.getItem(storageKey);
                if(oldWons !== null)
                {
                    var previousVersionWons = {};
                    previousVersionWons[storageKey] = oldWons;
                
                    storage.set(previousVersionWons);
                }
            }
			
			if(callback !== null)
			{
				callback();
			}
		});	
	}
	
	function setupUI()
	{
		$("head").append("<style type='text/css'>"+fontBootstrapper+"</style>");
		$("head").append("<link href='"+chrome.extension.getURL("font-awesome.min.css")+"' type='text/css' rel='stylesheet' />");
		$("body").prepend(wonListHtml);
		
		$("body").on("click", ".won-add", powertoys.events.addWonClick);
		$("body").on("click", ".insert-won", powertoys.events.insertWonClick);
		$("body").on("click", ".remove-won", powertoys.events.removeWonClick);
		$("body").on("click", ".rename-won", powertoys.events.renameWonClick);
		$("body").on("click", ".configure-won", powertoys.openConfigMenu);
		renderAllWons();
	}
	
	function init(){
		refreshWonList(function(){
			setupUI();
		});
	}

	// Export public interface
	var api = {};
	
	api.initialize = init;
	api.events = { 
		insertWonClick : insertWonEvent,
		addWonClick : addWonEvent,
		removeWonClick : removeWonEvent,
		renameWonClick : renameWonEvent,
	};
	api.openConfigMenu = openConfigureMenu;
	api.render = renderAllWons;
	
	window.powertoys = api;
	
})(jQuery);

if (typeof window.name !== 'undefined' && window.name && window.name == "frmTimeCardEdit" && $('body > form[name="frmTimeCardEdit"]').size() > 0) {
	powertoys.initialize();
}